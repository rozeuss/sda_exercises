package pl.sdacademy.exercises;

public class Starter {

    public static void main(String[] args) {
        final Dog dog = new Dog();
        //TypObiektu nazwaObiektu = wywolanie konstruktora Typu (konstruktor ze wszystkimi parametrami)
        Dog pimpek = new Dog("Pimpek");
        pimpek.getName();
        pimpek.setName("Pimpus");
    }

}
